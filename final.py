

import getpass
import subprocess
import MySQLdb

class Customer:
    count=0
    customer_id=0
    customer_balance=0
    
    def _init_(self):
        count+=1
        
    def personal_details(self):
        print('\n\n\n\t\t\t\t\t\t\t\t****PLEASE ENTER YOUR PERSONAL DETAILS*****')
        name=raw_input('\n\n\n\tEnter your Name: ')
        self.name=name
        subprocess.call("cls", shell=True)
        state=raw_input('Enter your State: ')
        self.state=state
        city=raw_input('Enter your City: ')
        self.city=city
        address=raw_input('Enter your House NO., Street name: ')
        self.address=address
        subprocess.call("cls", shell=True)


        flag='0'
        print('1. Savings\t 2. Current')
        while(flag!='set'):
            try:
                account_type=input('Please Choose Your Account Type: ')
                if(account_type==1):
                    self.account_type='savings'
                    flag='set'
                elif(account_type==2):
                    self.account_type='current'
                    flag='set'
                    print('For Current Account You Must Have Rs.5000 in your account!')
                    first_deposit=0
                    while(first_deposit<5000):
                        first_deposit=input('Enter The Amount To Be Deposited In Your Account: ')
                        subprocess.call("cls", shell=True)
                        print('Deposit Is Not Sufficient!')
                    subprocess.call("cls", shell=True)
                    print('Transaction Successful!')
                    self.customer_balance=first_deposit
                else:
                    print('Pleae enter a valid choice')
            except:
                print('Please enter a valid choice')

        flag='0'
        while(flag!='set'):
            try:
                pin=int (input('Enter your PIN code: '))
                if(pin>100000 and pin<999999):
                    flag='set'
                else:
                    print('Please enter a valid 6 digit PIN number!')
            except:
                print('Please enter only numbers!')
        subprocess.call("cls", shell=True)
        self.pin=pin


        flag='0'
        while(flag!='set'):
            try:
                phone=raw_input('Enter your phone number: +91')
                phone=int(phone)
                if(phone>1000000000 and phone<9999999999):
                    flag='set'
                else:
                    print('Please enter a valid phone number!')
            except:
                print('Please enter only numbers!')
        subprocess.call("cls", shell=True)
        self.phone=phone


        email=raw_input('Enter your email-id: ')
        subprocess.call("cls", shell=True)
        self.email=email


        flag='0'
        while(flag!='set'):
            try:
                sex=raw_input('Enter your gender:(M/F) ')
                if(len(sex)==1):
                    gender=sex.lower()
                    if(gender=='m' or gender=='f'):
                        flag='set'
                    else:
                        print('Please enter only initials of your gender nothing else!')
                else:
                    print('Please enter only M or F!')
            except:
                print('Please enter information in given format!')
        subprocess.call("cls", shell=True)
        self.sex=sex


        flag='0'
        while(flag!='set'):
            try:
                age=int(input('Enter your age: '))
                if(age<120):
                    flag='set'
                else:
                    print('Please enter valid age only!')
            except:
                print('Please enter your age in numbers only!')
        subprocess.call("cls", shell=True)
        self.age=age

        
        flag='0'
        while(flag!='set'):
            try:
                password=getpass.getpass('Create a password: ')
                if(len(password)<7):
                    print('Your password must be 8 characters long!')
                    print('\nPlease try again')
                else:
                    flag='set'
            except:
                print('This is a bad password!')
        self.password=password
        subprocess.call("cls", shell=True)


    def account_no(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('insert into admin (customer_id) values("%d")' %(self.temp_id))
        db.commit()
        cursor.execute('select account_no from admin where customer_id="%s"' %(self.temp_id))
        account_tuple=cursor.fetchone()
        self.account_no=account_tuple[0]
        db.commit()
        db.close()
        print('\n\t\tYour Account Number is: %d'  %self.account_no)
        
    def add_balance(self):
        balance=input('Enter the amount to add: ')
        self.customer_balance=balance


    def submit_details(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('insert into customer_details (name, address, phone_no, email_id, state, city, pin, account_balance, gender, password, account_type)\
        values("%s", "%s", "%s", "%s", "%s", "%s", "%d", "%d", "%c", "%s", "%s")' \
        % (self.name, self.address, self.phone, self.email, self.state, self.city, self.pin, self.customer_balance, self.sex, self.password, self.account_type ))
        db.commit()
        
        cursor.execute('select customer_id from customer_details where name="%s"' %(self.name))
        fetched_temp_id=cursor.fetchone()
        self.temp_id=fetched_temp_id[0]
        db.close()


    def generate_id(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('select customer_id from customer_details where name="%s"' % self.name)
        customer_id=cursor.fetchone()
        self.customer_id=customer_id[0]
        db.close()
        

    def user_authentication(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        self.check_for_blocked_account='0'
        self.authentication_except='0'
        try:
                cursor.execute('select account_no from blocked_accounts where account_no="%d"' %self.customer_id_input)
                self.d=cursor.fetchone()
                if(self.d!=None):
                    self.check_for_blocked_account='set'
                else:
                    self.check_for_blocked_account='0'
                if(self.check_for_blocked_account=='set'):
                    print('Your Account Is Blocked!')
                else:
                    try:
                        cursor.execute('select password from customer_details where customer_id="%d"' %self.customer_id_input)
                        self.fetched_passwd_tuple=cursor.fetchone()
                        self.fetched_passwd=self.fetched_passwd_tuple[0]

                        cursor.execute('select name from customer_details where customer_id="%d"' %self.customer_id_input)
                        self.name_tuple=cursor.fetchone()
                        self.customer_name=self.name_tuple[0]
                        db.close()

                    except:
                        
                        self.authentication_except='set'

        except:
            print('Your Customer_ID is Wrong!')


    def block_account(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('insert into blocked_accounts (account_no) values("%d")' %self.customer_id_input)
        db.commit()
        db.close()


    def address_change(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('update customer_details set address ="%s" where customer_id="%s"' % (self.new_address, self.customer_id_input))
        db.commit()
        db.close()
        print('Your Address has Successfully been Changed!')


    def money_deposit(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('select account_balance from customer_details where customer_id="%s"' %self.customer_id_input)
        self.fetched_customer_balance=cursor.fetchone()
        self.old_balance=self.fetched_customer_balance[0]
        self.new_balance=self.old_balance + self.deposit_amount
        
        cursor.execute('update customer_details set account_balance ="%d" where customer_id="%s"' % (self.new_balance, self.customer_id_input))
        db.commit()

        cursor.execute('insert into customer_transactions (customer_id, trans_type, trans_amount) values("%d", "%s", "%d")' %(self.customer_id_input, "credit", \
                                                                                                                              self.deposit_amount))
        db.commit()
        db.close()
        print('Your money has successfully been credited!')


    def money_withdraw(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('select account_balance from customer_details where customer_id="%s"' %self.customer_id_input)
        self.fetched_customer_balance=cursor.fetchone()
        self.old_balance=self.fetched_customer_balance[0]
        self.new_balance=self.old_balance - self.withdraw_amount
        if(self.old_balance>self.withdraw_amount):
            cursor.execute('update customer_details set account_balance ="%d" where customer_id="%s"' % (self.new_balance, self.customer_id_input))
            db.commit()
            cursor.execute('insert into customer_transactions (customer_id, trans_type, trans_amount) values("%d", "%s", "%d")' %(self.customer_id_input, "debit", \
                                                                                                                              self.withdraw_amount))
            db.commit()
            db.close()
            print('Money withdrawl successful!')

        else:
            print('You Do Not Have Sufficient Balance in Your Account!')


    def print_statement(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('select * from customer_transactions where customer_id="%d"' % self.customer_id_input)
        self.row=cursor.fetchall()
        print('customer_id \t transaction_type \t date \t trans_amount\n')
        for i in range(len(self.row)):
            print('%s \t\t %s \t\t %s \t\t %s' % (self.row[i][0],self.row[i][1],self.row[i][2],self.row[i][3]))



    def money_transfer(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        self.account_verification='0'
        try:
            cursor.execute('select account_no from admin where account_no="%d"' %(self.account_to))
            self.account_verification='good'
       #     print('account_to: %s' %self.account_to)
            # THIS IS FOR ADDING AMOUNT TO THE BENIFICIARY ACCOUNT
            cursor.execute('select customer_id from admin where account_no="%d"' %(self.account_to))
         #   print('inside another select: %d' %self.account_to)
            self.benificiary_account_tuple=cursor.fetchone()
            self.benificiary_customer_id=self.benificiary_account_tuple[0]
        #    print('benificiary_ac_id: %d ' %self.benificiary_customer_id)
            cursor.execute('select account_balance from customer_details where customer_id="%d"' %self.benificiary_customer_id)
            self.fetched_customer_balance=cursor.fetchone()
            self.old_balance=self.fetched_customer_balance[0]
            self.new_balance=self.old_balance + self.transfer_amount
         #   if(self.old_balance>=self.transfer_amount):
            cursor.execute('update customer_details set account_balance ="%d" where customer_id="%d"' % (self.new_balance, self.benificiary_customer_id))
            db.commit()
            cursor.execute('insert into customer_transactions (customer_id, trans_type, trans_amount) values("%d", "%s", "%d")' %(self.benificiary_customer_id,\
                                                                                                                                      "credit", \
                                                                                                                              self.transfer_amount))
            print('So far so good')
            # THIS IS FOR TAKIN OUT MONEY FROM THE ONE WHO IS TRANSFERRING MONEY
        
            cursor.execute('select account_balance from customer_details where customer_id="%s"' %self.customer_id_input)
            self.fetched_customer_balance=cursor.fetchone()
            self.old_balance=self.fetched_customer_balance[0]
            self.new_balance=self.old_balance - self.transfer_amount
        
            if(self.old_balance>=self.transfer_amount):
                cursor.execute('update customer_details set account_balance ="%d" where customer_id="%s"' % (self.new_balance, self.customer_id_input))
                cursor.execute('insert into customer_transactions (customer_id, trans_type, trans_amount) values("%d", "%s", "%d")' %(self.customer_id_input, "debit",\
                                                                                                                              self.transfer_amount))
                db.commit()
                db.close()
                print('Money Transfer Successful!')

            
            else:
                print('You Do Not Have Sufficient Balance in Your Account!')

        except:
            self.account_verification='0'
            print('Account_number Is Not Valid!')



    def close_account(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('insert into deleted_accounts (account_no) values("%s")' %(self.customer_id_input))
        db.commit()
        cursor.execute('delete from customer_details where customer_id="%s"' %(self.customer_id_input))
        cursor.execute('delete from admin where customer_id="%s"' %(self.customer_id_input))
        cursor.execute('delete from customer_transactions where customer_id="%s"' %(self.customer_id_input))
        db.commit()
        db.close()
        print('Your account has been deleted\n You can no longer logIn to your account!')

    def admin_signin(self):
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        cursor.execute('select * from admin_credentials')
        credentials=cursor.fetchall()
        self.admin_id=credentials[0][0]
        self.admin_passwd=credentials[0][1]
        db.close

    def show_deleted_accounts(self):
        subprocess.call("cls", shell=True)
        db=MySQLdb.connect(host="localhost", user="root", passwd="root", db="bank_database", port=3300)
        cursor=db.cursor()
        print('\n\n\t\tThis is the list of Closed accounts:')
        print('\n\t\tAccount_No\t\t\t Closing_Date\n')
        cursor.execute('select * from deleted_accounts')
        self.fetched_accounts=cursor.fetchall()
        for i in range(len(self.fetched_accounts)):
            print( '\t\t%s\t\t\t\t %s'%(self.fetched_accounts[i][0], self.fetched_accounts[i][1]))
        
        
        
user=1
while(user!=0 or user!=4):
    subprocess.call("cls", shell=True)
    print('\n\n\n\t\t\t\t\t\t\t\tWelcome to OUR Bank!\n\n\n')
    print('\t\t\tEnter 1 to signUp: ')
    print('\t\t\tEnter 2 to SignIn: ')
    print('\t\t\tEnter 3 to Admin SignIn')
    print('\t\t\tEnter 4 to QUIT')
    user=int(input('\n\t\t\t'))
    subprocess.call("cls", shell=True)

    if(user==1):
        
        cust1=Customer()
        cust1.personal_details()
        cust1.submit_details()
        cust1.generate_id()
        print('\n\t\tCongratulations!!! You have created an account.')
        cust1.account_no()
        print("\n\t\tPlease write down your customer ID")
        print("\n\t\tYour customer Id is: %d") % cust1.customer_id
        user=int(input("\n\n\n\tPress 1 To Go Back To Main Menu"))
    elif(user==2):
        cust1=Customer()
        customer_choice=0
        count=0
        while(count!=3):
            print('Enter your customer ID: ')
            cust1.customer_id_input= input()
            print('Enter your password: ')
            cust1.password_input=getpass.getpass()
            test=cust1.user_authentication()
            if(test==None):
                if(cust1.check_for_blocked_account=='set'):
                    cust1.check_for_blocked_account=='0'
                    raw_input()
                    break
                elif(cust1.check_for_blocked_account!='set'):
                    if(cust1.authentication_except=='set'):
                        print('Not a Valid Customer_ID!')
                    elif(cust1.password_input==cust1.fetched_passwd):
                        while(customer_choice!=7):
                            subprocess.call("cls", shell=True)
                            print('\n\n\t\t\t\t Welcome to our bank %s!!!' %cust1.customer_name)
                            print("\n\n\t\t\t\t What can we do for you?")
                            print('\n\n\t\t\t\t 1. Address Change')
                            print('\n\n\t\t\t\t 2. Money Deposit')
                            print('\n\n\t\t\t\t 3. Money Withdrawl')
                            print('\n\n\t\t\t\t 4. Print Statement')
                            print('\n\n\t\t\t\t 5. Transfer Money')
                            print('\n\n\t\t\t\t 6. Account Closure')
                            print('\n\n\t\t\t\t 7. Customer Logout')
                            customer_choice=input('\n\n    Please Enter Your Choice: ')
                            if(customer_choice==1):
                                subprocess.call("cls", shell=True)
                                print('Enter Your New Address: ')
                                cust1.new_address=raw_input()
                                cust1.address_change()

                            elif(customer_choice==2):
                                subprocess.call("cls", shell=True)
                                cust1.deposit_amount=input('Enter Amount To Deposit: ')
                                if(cust1.deposit_amount>0):
                                    cust1.money_deposit()
                                else:
                                    print('Invalid Amount!')

                            elif(customer_choice==3):
                                subprocess.call("cls", shell=True)
                                cust1.withdraw_amount=input('Enter Amount To Withdraw: ')
                                if(cust1.withdraw_amount>0):
                                    cust1.money_withdraw()
                                else:
                                    print('Invalid Amount!')

                            elif(customer_choice==4):
                                subprocess.call("cls", shell=True)
                                cust1.print_statement()

                            elif(customer_choice==5):
                                cust1.account_to=input('Enter Beneficiary Account_No: ')
                                cust1.transfer_amount=input('Enter Amount To be Transferred: ')
                                if(cust1.transfer_amount>0):
                                    cust1.money_transfer()
                                else:
                                    print('Invalid Amount!')

                            elif(customer_choice==6):
                                cust1.close_account()
                                count=3
                                raw_input()
                                break

                            elif(customer_choice==7):
                                subprocess.call("cls", shell=True)
                                print('\n\n\t\t\t\t\t\tYour have been Logged Off!')
                                count=3
                                raw_input()
                                break
        
                            customer_choice=input('\n\n\tEnter 1 to go to the Main Menu ')

                    else:
                        print('Your Password Seems to be Wrong!')
                        count+=1
                        if(count==3):
                            subprocess.call("cls", shell=True)
                            print('\n\n\n\n\t\t\t\t Your Account Has Been Blocked Due To Multiple Successive Errorneous Attempts!!!')
                            cust1.block_account()
                            print('\n\n\n\n\t\t\t\t\t\t\t\t Hit Enter!: ')
                            raw_input()
                        continue
            else:
                print('Value returned is not None')
    

    elif(user==3):
        cust1=Customer()
        cust1.admin_signin()
        try_id=raw_input('Enter Your ID: ')
        try_passwd=raw_input('Enter your Password: ')
        if(try_id==cust1.admin_id and try_passwd==cust1.admin_passwd):
            print('Welcome Administrator!')
            admin_choice=input('1. Show deleted accounts')
            if(admin_choice==1):
                cust1.show_deleted_accounts()
                a=raw_input()
        else:
            print('Your Credentials Are Wrong!')
            a=raw_input('Press Press Enter!')

    
    elif(user==4):
        print('\n\n\n\n\t\t\t\t\t\t\t\tThank You!')
        print('\n\t\t\t\t\t\t\t  Please visit us again!')
        break
